# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Debian Installer master translation file template
# Don't forget to properly fill-in the header of PO files
# Debian Installer translators, please read the D-I i18n documentation
# in doc/i18n/i18n.txt
# Translations from iso-codes:
# Alastair McKinstry <mckinstry@debian.org>, 2004.
# Priti Patil <prithisd@gmail.com>, 2007.
# Sampada Nakhare, 2007.
# Sandeep Shedmake <sshedmak@redhat.com>, 2009, 2010.
# localuser <sampadanakhare@gmail.com>, 2015.
# Nayan Nakhare <nayannakhare@rediffmail.com>, 2018.
# Prachi Joshi <josprachi@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: flash-kernel@packages.debian.org\n"
"POT-Creation-Date: 2019-09-26 22:05+0000\n"
"PO-Revision-Date: 2019-12-11 16:47+0000\n"
"Last-Translator: Prachi Joshi <josprachi@yahoo.com>\n"
"Language-Team: CDAC_DI\n"
"Language: mr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"

#. Type: text
#. Description
#. This item is a progress bar heading when the system configures
#. some flashable memory used by many embedded devices
#. :sl4:
#: ../flash-kernel-installer.templates:1001
msgid "Configuring flash memory to boot the system"
msgstr "प्रणालीचा आरंभ होण्यासाठी फ्लॅश मेमरीची संरचना होत आहे"

#. Type: text
#. Description
#. This item is a progress bar heading when an embedded device is
#. configured so it will boot from disk
#. :sl4:
#: ../flash-kernel-installer.templates:2001
msgid "Making the system bootable"
msgstr "प्रणाली आरंभयोग्य करत आहे"

#. Type: text
#. Description
#. This is "preparing the system" to flash the kernel and initrd
#. on a flashable memory
#. :sl4:
#: ../flash-kernel-installer.templates:3001
msgid "Preparing the system..."
msgstr "प्रणाली तयार करत आहे..."

#. Type: text
#. Description
#. This is a progress bar showing up when the system
#. write the kernel to the flashable memory of the embedded device
#. :sl4:
#: ../flash-kernel-installer.templates:4001
msgid "Writing the kernel to flash memory..."
msgstr "कर्नेल फ्लॅश मेमरीवर लिहीत आहे..."

#. Type: text
#. Description
#. This is a progress bar showing up when the system generates a
#. special boot image on disk for some embedded device so they
#. can boot.
#. :sl4:
#: ../flash-kernel-installer.templates:5001
msgid "Generating boot image on disk..."
msgstr "बूट प्रतिमा डिस्कवर बनवत आहे..."

#. Type: text
#. Description
#. Main menu item
#. Translators: keep below 55 columns
#. This item is a menu entry for a step where the system configures
#. the flashable memory used by many embedded devices
#. (writing the kernel and initrd to it)
#. :sl4:
#: ../flash-kernel-installer.templates:6001
msgid "Make the system bootable"
msgstr "प्रणाली आरंभयोग्य करा"
